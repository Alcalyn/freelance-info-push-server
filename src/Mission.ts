export default class Mission {
    public url!: string;
    public jobTitle!: string;
    public location!: string;
    public duration!: string;
    public adr!: string;
    public startDate!: string;
    public description!: string;
}
