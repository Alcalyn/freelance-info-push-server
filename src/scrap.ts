require('dotenv').config();

import Scrapper from './Scrapper';
import Elastic from './Elastic';

//Scrapper.scrapMission('https://www.freelance-info.fr/offre-de-mission_691018.html').then(console.log);

const indexNewMissions = async () => {
    let hasNewMission: boolean;
    let page = 0;
    let added = 0;

    do {
        hasNewMission = false;
        ++page;
        console.log(`Indexing page ${page}...`);

        const missionPromises = await Scrapper.scrapPage(page);

        const indexPromises: Promise<any>[] = [];

        missionPromises.forEach(missionPromise => {
            missionPromise.then(mission => {
                const promise = Elastic.hasMissionByUrl(mission.url).then(alreadyExists => {
                    if (alreadyExists) {
                        console.log(mission.url + ' already exists, ignore');
                        return;
                    }

                    Elastic.indexMission(mission)
                        .then(() => {
                            ++added;
                            console.log(mission.url + ' added')
                        })
                        .catch(() => console.warn('Failed to index ' + mission.url))
                    ;

                    hasNewMission = true;
                });

                indexPromises.push(promise);
            });
        });

        await Promise.all(indexPromises);

        console.log(`${added} total missions added`);
    } while (hasNewMission || true);

    return Promise.resolve(added);
};

indexNewMissions().catch(console.error);
