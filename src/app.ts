require('dotenv').config();

import express from 'express';
import cors from 'cors';
import Elastic from './Elastic';

const app = express();
const port = 3000;

app.use(cors());

app.get('/search/:term', (req, res) => {
    Elastic.searchMissions(req.params.term)
        .then(result => result.body.hits.hits)
        .then(hits => res.send(hits))
        .catch(e => res.status(500).send(e))
    ;
});

app.listen(port, () => {
    console.log(`Example app listening at http://0.0.0.0:${port}/search/php`)
});
