import { Client, RequestParams } from '@elastic/elasticsearch';
import Mission from './Mission';

if (!process.env.ELASTIC_HOST) {
    throw new Error('env var ELASTIC_HOST must be set');
}

if (!process.env.ELASTIC_INDEX) {
    throw new Error('env var ELASTIC_INDEX must be defined');
}

const MISSIONS_INDEX = process.env.ELASTIC_INDEX;

console.log('Connecting to ' + process.env.ELASTIC_HOST + '...');
const client = new Client({ node: process.env.ELASTIC_HOST });
console.log('Connected.');

/**
 * Add a new mission in elastic search index.
 */
const indexMission = async (mission: Mission): Promise<void> => {
    const missionDoc: RequestParams.Index = {
        index: MISSIONS_INDEX,
        id: mission.url,
        body: {
            ...mission,
        },
    };

    try {
        await client.index(missionDoc);
    } catch (e) {
        console.error(e);
    }
};

const hasMissionByUrl = async (url: string) => {
    const params: RequestParams.Get = {
        index: MISSIONS_INDEX,
        id: url,
    };

    try {
        const result = await client.get(params);

        return result.body.found;
    } catch (e) {
        return Promise.resolve(false);
    }
};

/**
 * Perform a search from a term.
 */
const searchMissions = async (searchTerm: string) => {
    const params: RequestParams.Search = {
        index: MISSIONS_INDEX,
        body: {
            query: {
                bool: {
                    should: [
                        {
                            fuzzy: {
                                jobTitle: {
                                    value: searchTerm,
                                    boost: 2,
                                },
                            },
                        },
                        {
                            fuzzy: {
                                description: {
                                    value: searchTerm,
                                    boost: 1,
                                },
                            },
                        },
                    ],
                },
            },
        },
    };

    return client.search(params);
};

export default {
    indexMission,
    hasMissionByUrl,
    searchMissions,
};
