import axios from 'axios';
import { JSDOM } from 'jsdom';
import Mission from './Mission';

const scrapMission = async (url: string): Promise<Mission> => {
    const mission = new Mission();
    mission.url = url;

    const response = await axios.get(url);

    const dom = new JSDOM(response.data);
    const document = dom.window.document;

    const h1 = document.querySelector('h1');

    if (null === h1 || null === h1.textContent) {
        throw new Error('Mission has no job title');
    }

    mission.jobTitle = h1.textContent;

    const details = document.querySelector('.cf .col-half');

    if (null !== details && null !== details.textContent) {
        const regex = /.*: ?(.+)/gm;
        const values: string[] = [];

        for (let i = 0; i < 4; ++i) {
            const value = regex.exec(details.textContent);

            if (null === value) {
                break;
            }

            values.push(value[1]);
        }

        if (4 === values.length) {
            mission.location = values[0];
            mission.duration = values[1];
            mission.adr = values[2];
            mission.startDate = values[3];
        }
    }

    const description = document.evaluate('//h2[contains(., "Description")]', document, null, 0).iterateNext()?.parentNode?.textContent || '';
    const cleanDescription = description.match(/Description :(.*)Clic2search/sm);
    mission.description = cleanDescription ? cleanDescription[1].trim() : description;

    return mission;
};

const scrapPage = async (page: number = 1): Promise<Promise<Mission>[]> => {
    if (page < 1) {
        throw new Error('Page is not expected to be less than 1, got ' + page);
    }

    const url = 'https://www.freelance-info.fr/missions.php?p=' + page;

    const response = await axios.get(url);
    console.log(url, response.status);
    const dom = new JSDOM(await response.data);
    const document = dom.window.document;

    const missionPromises: Promise<Mission>[] = [];

    document.querySelectorAll('.roffre a.rtitre').forEach(links => {
        const missionPath = links.getAttribute('href');
        if (null !== missionPath) {
            const missionUrl = 'https://www.freelance-info.fr/' + missionPath;
            const promise = scrapMission(missionUrl);
            promise.catch(() => {
                console.warn('Failed to scrap ', missionUrl);
            });
            missionPromises.push(promise);
        }
    });

    return Promise.resolve(missionPromises);
};

export default {
    scrapMission,
    scrapPage,
}
